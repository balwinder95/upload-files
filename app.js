var express = require('express');
var multer = require('multer');
var app = express();
var port = require('./config/localDevelopment').PORT;
var routes = require('./routes/index');
var bodyParser = require('body-parser');
var path = require('path');
var sendResponse = require('./routes/sendResponse');
var done = false;

app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jade');

app.use(multer({ dest: './uploads/',
 rename: function (fieldname, filename) {
    return filename;
  },
onFileUploadStart: function (file) {
  console.log(file.originalname + ' is starting ...')
},
onFileUploadComplete: function (file) {
  console.log(file.fieldname + ' uploaded to  ' + file.path)
  done=true;
}
}));


app.get('/test', routes);

app.post('/file_uploaded', function (req,res){
  if(done == true){
    var data = req.files;
    sendResponse.sendSuccessData(data, res);
    console.log(req.files);
  }
});

app.listen(port, function(){
    console.log("Working on port " + port);
});